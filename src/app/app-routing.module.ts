import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes : Routes = [
  {
    path: '',
    // component: AuthenticatedLayoutComponent,
    children: [
      {
        path: 'home',
        // canActivate: [AuthGuard],
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
      },
      {
        path: 'professional-exp',
        // canActivate: [AuthGuard],
        loadChildren: () => import('./pages/professional-exp/professional-exp.module').then(m => m.ProfessionalExpModule),
      },
      {
        path: 'contact-me',
        // canActivate: [AuthGuard],
        loadChildren: () => import('./pages/contact-me/contactme.module').then(m => m.ContactMeModule),
      }
      // {
      //   path: 'sample',
      //   // canActivate: [AuthGuard],
      //   loadChildren: () => import('./pages/sample/sample.module').then(m => m.SampleModule),
      // },
      // { path: '**', redirectTo: 'home'}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
