import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';
// import { CpfCnpjPipe } from './pipes/cpf-cnpj.pipe';

const MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  FlexLayoutModule,
  MaterialModule,
];

const PIPES: any = [
    // CpfCnpjPipe
];

@NgModule({
  declarations: [
    PIPES,
  ],
  imports: [
    MODULES
  ],
  exports: [
    MODULES,
    PIPES
  ],
  providers: [
    DatePipe
  ],
})

export class CoreModule { }