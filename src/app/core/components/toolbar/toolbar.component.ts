import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-teste',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class Toolbar implements OnInit {
  constructor() {}
  ngOnInit(): void {}

  menu = [
    { title: "Home", link: "home", icon: "menu" },
    { title: 'Projetos Pessoais', icon: "menu" },
    { title: "Experiência Profissional", link: "professional-exp", icon: "menu" },
    { title: 'Formação Acadêmica e Certificados', icon: "menu"},
    { title: 'Recruta+', icon: "menu"},
    { title: 'Habilidades', icon: "menu" },
    { title: "Me Contate", link: "contact-me", icon: "menu" },
  ];
}
