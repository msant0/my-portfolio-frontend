import { Component, OnInit } from '@angular/core';
import { TimelineElement } from './core/components/horizontal-timeline/timeline-element'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {
  title = 'Hey There!';

  constructor() { }

  ngOnInit() {
  }
}