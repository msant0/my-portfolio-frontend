import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from 'src/app/core/core.module';
import { HomeComponent } from './home.component';
// import { HorizontalTimelineComponent } from '../../core/components/horizontal-timeline/horizontal-timeline.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
];

@NgModule({
  imports: [CoreModule, RouterModule.forChild(routes), RouterModule],

  declarations: [HomeComponent],
})
export class HomeModule {}
