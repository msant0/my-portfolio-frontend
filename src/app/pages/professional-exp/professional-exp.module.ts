import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from 'src/app/core/core.module';
import { ProfessionalExpComponent } from './professional-exp.component';
import { HorizontalTimelineComponent } from '../../core/components/horizontal-timeline/horizontal-timeline.component';

const routes: Routes = [
  {
    path: '',
    component: ProfessionalExpComponent,
  },
];

@NgModule({
  imports: [CoreModule, RouterModule.forChild(routes), RouterModule],
  declarations: [ProfessionalExpComponent, HorizontalTimelineComponent],
})
export class ProfessionalExpModule {}