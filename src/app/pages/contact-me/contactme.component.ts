import { Component, OnInit } from "@angular/core";

@Component({
    selector: 'app-contact-me',
    templateUrl: './contactme.component.html',
    styleUrls: ['./contactme.component.scss'],
})
  
export class ContactMeComponent implements OnInit {
    constructor() {}

    name = "Nome";
    emailAddress = "Email de contato";

    labelMap="Criando linhas de código em São Paulo 🌆";
    contactMe="Hey, Mande uma mensagem!";
    subjectContact="Assunto";
    messageContact="Mensagem";
    btnSendMessage="Enviar Mensagem";
    headerContact = "Parcerias";
    numberConnection = "2";
    textConnection = "Conexões realizadas a partir do meio de contato";

    ngOnInit(): void {}
}