import { CoreModule } from './../../core/core.module';
import { NgModule } from '@angular/core';
import { ContactMeComponent } from './contactme.component';
import { RouterModule, Routes } from '@angular/router';
import { MapComponent } from 'src/app/map/map.component';

const routes: Routes = [
  {
    path: '',
    component: ContactMeComponent,
  },
];

@NgModule({
  imports: [
    CoreModule,
    RouterModule.forChild(routes),
    RouterModule,
  ],
  declarations: [ContactMeComponent, MapComponent],
})

export class ContactMeModule {}
