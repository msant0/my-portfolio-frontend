# links
https://icons8.com/icons/set/developer -> free icons

# bootstrap

- margins and padding: <br>
Today 2021 in boostrap 5 Margin and Padding :
Margin
Margin top: mt-value
Margin right: me-value
Margin bottom: mb-value
Margin left: ms-value

Padding
Padding top: pt-value
Padding right: pe-value
Padding bottom: pb-value
Paddig left: ps-value.
Where the range for the value is 0 to 5

- https://icons.getbootstrap.com -> bootsstrap icons

# svgs
- https://fonts.google.com/icons?icon.query=arrow -> google

# condicionais 

## angular
<div *ngIf="i != menu.length -1; then thenBlock else elseBlock"></div> | <ng-template  #thenBlock>|</ng-template> -> if/else


# docker
- docker rm $(docker ps -aq) -> remover container
- docker ps -> listar todos os containers

## css
- para que serve ol e ul?

- para que serve  -webkit-transition: opacity 0.2s;?

- para que serve  -moz-transition?

- para que serve -webkit-transform: scaleX(0);?

- para que serve -mox-transform: translateX(-50%);?

- para que serve changeDetection: ChangeDetectionStrategy.OnPush

- para que serve window.getComputedStyle(timeline);
